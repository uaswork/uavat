#!/bin/bash

ln -s /home/$USER/uavat_ws/src/uavat/uavat_gazebo/init.d-posix/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/init.d-posix/
ln -s /home/$USER/uavat_ws/src/uavat/uavat_gazebo/mixers/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/mixers/
ln -s /home/$USER/uavat_ws/src/uavat/uavat_gazebo/models/* /home/$USER/PX4-Autopilot/Tools/sitl_gazebo/models/
