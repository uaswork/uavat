# UAVAT Framework: Gazebo

![Screenshot](images/sitl.png)

## Prerequisite installations and configurations

### Robot Operating System (ROS)
Follow the guide from [the official ROS](http://wiki.ros.org/Distributions) webpage (depending on your Linux/Ubuntu distribution).

### PX4 Firmware

#### Installation
Clone the PX4 firmware (v1.11.3) from github

```sh
cd ~
git clone https://github.com/PX4/PX4-Autopilot
cd ~/PX4-Autopilot
git checkout v1.11.3
git submodule update --init --recursive
```

Build the PX4 SITL firmware and run the Gazebo environment
```sh
cd ~/PX4-Autopilot
DONT_RUN=1 make px4_sitl_default gazebo
```

Install dependencies for PX4 SITL
```sh
cd ~/PX4-Autopilot_old/Tools/setup/
./ubuntu.sh
```

### UAVAT Gazebo

Add the commands to your `.bashrc`
```sh
source /home/$USER/PX4-Autopilot/Tools/setup_gazebo.bash /home/$USER/PX4-Autopilot /home/$USER/PX4-Autopilot/build/px4_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/home/$USER/PX4-Autopilot
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/home/$USER/PX4-Autopilot/Tools/sitl_gazebo
```
(and source the `.bashrc`)

Source the UAVAT Workspace
```sh
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/home/$USER/uavat_ws/src/uavat/uavat_gazebo/models
```

or, run

```sh
source ~/uavat_ws/src/uavat/uavat_gazebo/setup_gazebo.bash
```

### Symlink the UAVAT Framework files into your PX4 SITL environment
Symlink the UAVAT Framework airframes, models and mixer files into your the 'PX4-Autopilot' folder,

```sh
ln -s /home/$USER/uavat_ws/src/uavat/uavat_gazebo/init.d-posix/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/init.d-posix/
ln -s /home/$USER/uavat_ws/src/uavat/uavat_gazebo/mixers/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/mixers/
ln -s /home/$USER/uavat_ws/src/uavat/uavat_gazebo/models/* /home/$USER/PX4-Autopilot/Tools/sitl_gazebo/models/
```

or, run

```sh
source ~/uavat_ws/src/uavat/uavat_gazebo/setup_posix.bash
```

### (Optional) Setup alias for your 

Add the commands to your `.bashrc`.
```sh
alias suavat-ws="source ~/uavat_ws/devel/setup.zsh && source ~/uavat_ws/src/uavat/uavat_gazebo/setup_gazebo.bash"
alias puavat-ws="cd ~/uavat_ws/ && suavat-ws"
alias buavat-ws="puavat-ws && catkin build"
```
where 
* `suavat-ws` is a shortcut for sourcing the UAVAT Framework workspace and the PX4-Autopilot firmware
* `puavat-ws` is a shortcut for sourcing both projects and go to the UAVAT Framework workspace
* `buavat-ws` is a shortcut for building, sourcing both projects and go to the UAVAT Framework workspace

### Start UAVAT Framework SITL environment
Start 
```sh
roslaunch uavat_bringup bringup_uav_sitl.launch
```

