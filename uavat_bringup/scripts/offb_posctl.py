#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
offb_posctl.py: Controlling the setpoints for each Test Module

"""

###############################################
# Standard Imports                            #
###############################################
import time
import threading
import math
import numpy as np

import csv
import os

from scipy.spatial import distance

from shapely.geometry import Polygon, Point, LinearRing

###############################################
# ROS Imports                                 #
###############################################
import rospy
import rospkg

###############################################
# ROS Topic messages                          #
###############################################
from std_msgs.msg import String, Float32

from geometry_msgs.msg import PoseStamped

from sensor_msgs.msg import NavSatFix

from mavros_msgs.msg import State

###############################################
# ROS Service messages                        #
###############################################
from mavros_msgs.srv import CommandBool, SetMode, CommandTOL
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
from uavat_bringup.srv import UAVState, UAVStateRequest, UAVStateResponse


###############################################
# Offboad Control class                       #
###############################################
class OffboardControl:
    def __init__(self, *args):
        self.current_state = State()
        self.current_local_pose = PoseStamped()

        rospy.init_node('offboard_ctrl')
        self.rate = rospy.Rate(10)

        self.offb_set_mode = SetMode

        self.prev_state = self.current_state

        # Publishers
        self.local_pos_pub = rospy.Publisher('/mavros/setpoint_position/local', PoseStamped, queue_size=10)
        self.state_pub = rospy.Publisher('/mavros/offbctrl/state', String, queue_size=10)

        # Subscribers
        self.state_sub = rospy.Subscriber('/mavros/state', State, self.cb_mavros_state)
        self.sub_target = rospy.Subscriber('/mavros/offbctrl/target', PoseStamped, self.cb_target)
        self.pose_sub = rospy.Subscriber('/mavros/local_position/pose', PoseStamped, self.cb_local_pose, queue_size = 1)

        # Service
        self.arming_client = rospy.ServiceProxy('/mavros/cmd/arming', CommandBool)
        self.takeoff_client = rospy.ServiceProxy('/mavros/cmd/takeoff', CommandTOL)
        self.set_mode_client = rospy.ServiceProxy('/mavros/set_mode', SetMode)

        ## Create services
        self.create_services()

        # Init msgs
        self.target = PoseStamped()
        self.target.pose.position.x = 0
        self.target.pose.position.y = 0
        self.target.pose.position.z = 1

        self.last_request = rospy.get_rostime()
        self.state = "INIT"

        self.rate = rospy.Rate(20.0) # MUST be more then 2Hz

        self.t_run = threading.Thread(target=self.navigate)
        self.t_run.start()
        print(">> SetPoint controller is running (Thread)")

        # Spin until the node is stopped

        #time.sleep(5)
        #tmp = Empty()
        #self.switch2offboard(tmp)

        while not rospy.is_shutdown():
            current_status = self.get_state()
            self.state_pub.publish(current_status)
            self.rate.sleep()

    """
    Callbacks
    * cb_mavros_state: Receive the FC's current state
    * cb_target: Receive and update the target local setpoint
    * cb_local_pose: Receive the sUAV's local position
    """
    def cb_mavros_state(self,state):
        self.current_state = state

    def cb_target(self,data):
        self.set_target(data)

    def cb_local_pose(self,data):
        self.current_local_pose = data
        #print(self.current_local_pose)

    """
    Services
    * s_state: Change the sUAS's current state
    """
    def create_services(self):
        s_state = rospy.Service('setpoint_controller/state', UAVState, self.set_status)

        print("The service(s) is ready")


    """
    State
    * set_state: Set current state
    * get_state: Get current state
    """
    def set_state(self, data):
        self.state = data
        print("New State: {}".format(data))

    def get_state(self):
        return self.state

    """
    Target position
    * set_target: Set new local target setpoint
    * set_target_xyz: Set new local target setpoint (defined by only x,y and z)
    * get_target: Get current local target setpoint
    """
    def set_target(self, data):
        self.target = data

    def set_target_xyz(self,x,y,z,delay):

        #if(delay > 0.1):
        #    print(">> New setpoint: {} {} {}".format(x,y,z))

        self.target.pose.position.x = x
        self.target.pose.position.y = y
        self.target.pose.position.z = z

        time.sleep(delay)

    def get_target(self):
        return self.target

    """
    Commands
    * set_status: Set new state of the sUAS
    * navigate: Keep publishing local positions to keep the OFFBOARD active
    * stop: Set current state of the sUAS to STOP
    """
    def set_status(self,r):
        #print(">> New status: "+r.status)
        self.set_state(r.state)

        return UAVStateResponse(
            success=True,
            message="New state: "+r.state
        )

    def navigate(self):
        self.set_state("RUNNING")
        while not rospy.is_shutdown():
            self.target.header.frame_id = "base_footprint"
            self.target.header.stamp = rospy.Time.now()

            self.local_pos_pub.publish(self.target)
            self.rate.sleep()

        print(">> Navigation thread has stopped...")
        self.set_state("STOP")

    def stop(self,r):
        self.set_target_xyz(0,0,1,0.5)
        self.set_state("STOP")
        return {}

if __name__ == '__main__':
    SPC = OffboardControl()
