# UAVAT Framework: Bringup

## Offboard control (only)
```sh
rosrun uavat_bringup offb_posctl.py
```

# Endurance bringup
See [link](../uavat_endurance_test/README.md)
## PX4 SITL
```sh
roslaunch uavat_bringup bringup_endurance_sitl.launch
```

## Real-world
```sh
roslaunch uavat_bringup bringup_endurance.launch
```


# Fault Detection bringup
See [link](../uavat_fd_test/README.md)

## PX4 SITL
```sh
roslaunch uavat_bringup bringup_fd_sitl.launch
```

## Real-world
```sh
roslaunch uavat_bringup bringup_fd.launch
```

