# UAVAT Framework: UAV Auto Test Framework for Experimental Validation of Multirotor sUAS

![Screenshot](uavat.png)

# Prerequisite installations and configurations

## Robot Operating System (ROS)
Follow the guide from [the official ROS](http://wiki.ros.org/Distributions) webpage (depending on your Linux/Ubuntu distribution).

## MAVROS
Install following [MAVROS](http://wiki.ros.org/mavros) packages
```sh
sudo apt install ros-<your-ros-distro>-mavros ros-<your-ros-distro>-mavros-extras -y
```

and, install GeographicLib datasets by running the `install_geographiclib_datasets.sh` script:

```sh
cd ~
wget https://raw.githubusercontent.com/mavlink/mavros/master/mavros/scripts/install_geographiclib_datasets.sh
chmod 755 install_geographiclib_datasets.sh
sudo ./install_geographiclib_datasets.sh
```

## UAVAT workspace

### Prerequisite installations
Install the [Catkin Command Line Tools](https://catkin-tools.readthedocs.io/en/latest/) package
```sh
pip3 install catkin-tools
```

### Create your UAVAT workspace
```sh
cd ~
mkdir -p uavat_ws/src
cd ~/uavat_ws/src
catkin_init_workspace
cd ..
catkin build
```

Clone and build the UAVAT repo

```sh
cd ~/uavat_ws/src
git clone git@gitlab.com:uaswork/uavat.git
catkin build
```

# Future work
The following will be implemented/improved in the future:

* **[Endurance test]:** More flight geometries
  * List of local waypoints
  * Sphere flight path
* Enable positional error/noise in the local positions


