#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
failure_detection.py: Failure Detection (FD) Test Module.

"""

###############################################
# Standard Imports                            #
###############################################
import time
import threading
import math
import numpy as np

import csv
import os

from scipy.spatial import distance

from shapely.geometry import Polygon, Point, LinearRing

###############################################
# ROS Imports                                 #
###############################################
import rospy
import rospkg

###############################################
# ROS Topic messages                          #
###############################################
from std_msgs.msg import String, Float32

from geometry_msgs.msg import PoseStamped

from sensor_msgs.msg import NavSatFix

from mavros_msgs.msg import State

###############################################
# ROS Service messages                        #
###############################################
from mavros_msgs.srv import CommandBool, SetMode, CommandTOL
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
from uavat_bringup.srv import UAVState, UAVStateRequest, UAVStateResponse

###############################################
# Offboad Control class                       #
###############################################
class FailureDetection:
    def __init__(self, *args):
        rospy.init_node('failure_detection')

        self.current_state = State()

        # Mahalanobis Distance
        self.rospack = rospkg.RosPack()
        self.path = self.rospack.get_path('uavat_fd_test')

        print("Path: {}".format(self.path))

        self.iv = np.identity(4)
        self.mean_vals = [0, 0, 0, 0]

        self.geometry = ""

        self.mahal_dist = Float32()

        # Path Square
        self.poly = Polygon([(0.5, 0.5), (0.5, -0.5), (-0.5, -0.5), (-0.5, 0.5)])

        # Path (cirle)
        self.target_x = 0
        self.target_y = 0
        self.target_z = 1

        self.radius = 1

        # Publishers
        self.mahal_dist_pub = rospy.Publisher('/mahalanobis/dist', Float32, queue_size=10)

        # Subscribers
        self.state_sub = rospy.Subscriber('/mavros/state', State, self.cb_mavros_state)
        self.state_sub = rospy.Subscriber('/mavros/offbctrl/state', String, self.cb_state)
        self.state_sub = rospy.Subscriber('/mavros/local_position/pose', PoseStamped, self.cb_local_pose, queue_size = 1)

        # Services
        self.set_mode_client = rospy.ServiceProxy('/mavros/set_mode', SetMode)

        ## Create services
        self.create_services()

        # Init msgs
        self.target = PoseStamped()
        self.target.pose.position.x = 0
        self.target.pose.position.y = 0
        self.target.pose.position.z = 1

        self.last_request = rospy.get_rostime()
        self.state = "INIT"

        self.rate = rospy.Rate(20.0) # MUST be more then 2Hz

        print(">> SetPoint controller is running (Thread)")

        # Spin until the node is stopped

        #time.sleep(5)
        #tmp = Empty()
        #self.switch2offboard(tmp)

        rospy.spin()

    """
    Callbacks
    * cb_state: Receive the sUAV's current state
    * cb_local_pose: Receive the sUAS's local position
    * cb_mavros_state: Receive the FC's current state
    """
    def cb_state(self,state):
        #print(state.data)
        self.set_state(state.data)

    def cb_local_pose(self,data):
        self.current_local_pose = data

        #print(self.current_local_pose)

    def cb_mavros_state(self,state):
        self.current_state = state

    """
    State
    * set_state: Set current state
    * get_state: Get current state
    """
    def set_state(self, data):
        self.state = data
        #print("New State: {}".format(data))

    def get_state(self):
        return self.state

    """
    Services
    * s_fd: Start the Fault Detection
    * s_s2l: Command the sUAS to perform a AUTO.LAND
    """
    def create_services(self):
        s_fd = rospy.Service('setpoint_controller/fd', Empty, self.start_fd)
        s_s2l = rospy.Service('setpoint_controller/switch2land', Empty, self.switch2land)

        self.request_state = rospy.ServiceProxy('/setpoint_controller/state', UAVState)

        print("The service(s) is ready")

    """
    Commands
    * switch2land: Command the sUAS to perform a AUTO.LAND
    """
    def switch2land(self,r):
        print(">> Activating LAND mode")

        last_request = rospy.get_rostime()
        while self.current_state.mode != "AUTO.LAND":
            now = rospy.get_rostime()
            if(now - last_request > rospy.Duration(5)):
                print("Trying: LAND mode")
                self.set_mode_client(base_mode=0, custom_mode="AUTO.LAND")
                last_request = now

        print("Landing...")

        try:
            req = UAVStateRequest()
            req.state = "LANDING"
            self.request_state(req)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)

        return {}

    """
    Mahalanobis Distance
    * quaternion_to_euler: Convert quaternion to euloer
    * start_fd / fd :
    """
    # Quaternion and Euler conversion(s)
    def quaternion_to_euler(self, x=None, y=None, z=None, w=None):
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)

        roll = math.atan2(t0, t1)

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2

        pitch = math.asin(t2)

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)

        yaw = math.atan2(t3, t4)

        #print("RPY: {} {} {}".format(roll,pitch,yaw))
        return [roll,pitch,yaw]

    def start_fd(self, r):
        self.t_fd = threading.Thread(target=self.fd)
        self.t_fd.start()
        print(">> Starting FD (Thread)")

        return {}

    def fd(self):
        print(">> Starting FD")

        if self.state == "CIRCLE":
            self.geometry = "circle"
        elif self.state == "SQUARE":
            self.geometry = "square"
        elif self.state == "HOVER":
            self.geometry = "hover"
        else:
            self.geometry = "none"

        with open(self.path+'/mahal/'+self.geometry+'/errs_mahala.csv', 'r') as csvFile:
            reader = csv.reader(csvFile)
            next(reader)

            print("Geometry: {}".format(self.geometry))

            for row in reader:
                print(row)
                self.mean_vals[0] = float(row[0])
                self.mean_vals[1] = float(row[1])
                self.mean_vals[2] = float(row[2])
                self.mean_vals[3] = float(row[3])
            csvFile.close()

        print("Mahalanobis Distance means: {}".format(self.mean_vals))

        while self.state == "HOVER" or self.state == "CIRCLE" or self.state == "SQUARE":
            rpy = self.quaternion_to_euler(
                self.current_local_pose.pose.orientation.x,
                self.current_local_pose.pose.orientation.y,
                self.current_local_pose.pose.orientation.z,
                self.current_local_pose.pose.orientation.w)

            #print("Ch: {} {} {} {} {} {}  --> Pose {} {} {}  {} {} {}".format(
            #    self.current_rc_out.channels[0],
            #    self.current_rc_out.channels[1],
            #    self.current_rc_out.channels[2],
            #    self.current_rc_out.channels[3],
            #    self.current_rc_out.channels[4],
            #    self.current_rc_out.channels[5],
            #    self.current_local_pose.pose.position.x,
            #    self.current_local_pose.pose.position.y,
            #    self.current_local_pose.pose.position.z,
            #    rpy[0],
            #    rpy[1],
            #    rpy[2]))

            errs = 0.0

            xc = self.current_local_pose.pose.position.x
            yc = self.current_local_pose.pose.position.y
            zc = self.current_local_pose.pose.position.z

            if self.geometry == "circle":
                #print("Geometry (check): {}".format(self.geometry))
                p2xy = math.fabs(math.sqrt(xc*xc+yc*yc)-self.radius) #math.fabs(math.sqrt(float(row[4])+float(row[5])-radius))
                p2z = zc-self.target_z
                errs = math.sqrt(p2xy*p2xy+p2z*p2z)
            elif self.geometry == "square":
                point = Point(xc, yc)
                pol_ext = LinearRing(self.poly.exterior.coords)

                d = pol_ext.project(point)
                p = pol_ext.interpolate(d)

                closest_point_coords = list(p.coords)[0]

                p2x = closest_point_coords[0]-xc
                p2y = closest_point_coords[1]-yc
                p2z = zc-self.target_z

                errs = math.fabs(math.sqrt(p2x*p2x+p2y*p2y+p2z*p2z))
            else:
                #print("Geometry (check): {}".format(self.geometry))
                p2x = xc
                p2y = yc
                p2z = zc-self.target_z
                errs = math.fabs(math.sqrt(p2x*p2x+p2y*p2y+p2z*p2z))

            self.mahal_dist.data = distance.mahalanobis([errs, math.fabs(rpy[0]), math.fabs(rpy[1]), math.fabs(rpy[2])], self.mean_vals, self.iv)
            #print("Mahalanobis Distance: {} ".format(self.mahal_dist.data))

            self.mahal_dist_pub.publish(self.mahal_dist)

            if self.mahal_dist.data > 0.30:
                print(">> Thresh detected: {}".format(self.mahal_dist))
                self.state = "LANDING"
                print(">> Landing...")
                tmp = Empty()
                self.switch2land(tmp)

            time.sleep(0.1)



if __name__ == '__main__':
    FDtm = FailureDetection()
