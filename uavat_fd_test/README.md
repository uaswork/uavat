# UAVAT Framework: Fault Detection (FD) test

## Feature model
![Screenshot](images/feature_model.png)

## Fault Detection test (only)

```sh
rosrun uavat_fd_test failure_detection.py
```

## Bringup

### PX4 SITL
```sh
roslaunch uavat_bringup bringup_fd_sitl.launch
```

### Real-world
```sh
roslaunch uavat_bringup bringup_fd.launch
```

### Activate fault detection
```sh
rosservice call /setpoint_controller/fd "{}"
```

