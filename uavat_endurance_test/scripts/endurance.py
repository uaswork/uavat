#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
endurance.py: Endurance Test Module.

"""

###############################################
# Standard Imports                            #
###############################################
import time
import threading
import math
import numpy as np

import csv
import os

from scipy.spatial import distance

from shapely.geometry import Polygon, Point, LinearRing

###############################################
# ROS Imports                                 #
###############################################
import rospy
import rospkg

###############################################
# ROS Topic messages                          #
###############################################
from std_msgs.msg import String, Float32

from geometry_msgs.msg import PoseStamped

from sensor_msgs.msg import NavSatFix

from mavros_msgs.msg import State


###############################################
# ROS Service messages                        #
###############################################
from mavros_msgs.srv import CommandBool, SetMode, CommandTOL
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
from uavat_bringup.srv import UAVState, UAVStateRequest, UAVStateResponse

###############################################
# Offboad Control class                       #
###############################################
class Endurance:
    def __init__(self, *args):
        rospy.init_node('endurance')

        # Path Square
        self.poly = Polygon([(0.5, 0.5), (0.5, -0.5), (-0.5, -0.5), (-0.5, 0.5)])


        # Path (cirle)
        self.target_x = 0
        self.target_y = 0
        self.target_z = 1

        self.radius = 1

        # Publishers
        self.target_pub = rospy.Publisher('/mavros/offbctrl/target', PoseStamped, queue_size=10)

        # Subscribers
        self.state_sub = rospy.Subscriber('/mavros/offbctrl/state', String, self.cb_state)

        # Services

        ## Create services
        self.create_services()

        # Init msgs
        self.target = PoseStamped()
        self.target.pose.position.x = 0
        self.target.pose.position.y = 0
        self.target.pose.position.z = 1

        self.last_request = rospy.get_rostime()
        self.state = "INIT"

        self.rate = rospy.Rate(20.0) # MUST be more then 2Hz

        print(">> SetPoint controller is running (Thread)")

        # Spin until the node is stopped

        #time.sleep(5)
        #tmp = Empty()
        #self.switch2offboard(tmp)

        rospy.spin()

    """
    Callbacks
    * cb_state: Receive the sUAV's current state
    * cb_local_pose: Receive the sUAV's local position
    """
    def cb_state(self,state):
        #print(state.data)
        self.set_state(state.data)

    def cb_local_pose(self,data):
        self.current_local_pose = data

        #print(self.current_local_pose)

    """
    State
    * set_state: Set current state
    * get_state: Get current state
    """
    def set_state(self, data):
        self.state = data
        #print("New State: {}".format(data))

    def get_state(self):
        return self.state

    """
    Services
    * s_circle:
    * s_square:
    * s_stop:
    """
    def create_services(self):
        s_hover = rospy.Service('setpoint_controller/hover', Empty, self.start_hover)
        s_circle = rospy.Service('setpoint_controller/circle', Empty, self.start_circle)
        s_square = rospy.Service('setpoint_controller/square', Empty, self.start_square)
        s_stop = rospy.Service('setpoint_controller/stop', Empty, self.stop)

        self.request_state = rospy.ServiceProxy('/setpoint_controller/state', UAVState)

        print("The service(s) is ready")

    """
    Target position
    * set_target_xyz: Set new local target setpoint (defined by only x,y and z)
    """
    def set_target_xyz(self,x,y,z,delay):

        #if(delay > 0.1):
        #    print(">> New setpoint: {} {} {}".format(x,y,z))

        self.target.pose.position.x = x
        self.target.pose.position.y = y
        self.target.pose.position.z = z

        self.target_pub.publish(self.target)
        time.sleep(delay)


    """
    Commands
    * start_hover / hover: Start the HOVER test mode
    * start_circle / circle: Start the CIRCLE test mode
    * start_square / square: Start the CIRCLE test mode
    * stop: Stop the current test mode
    """

    def start_hover(self, r):
        try:
            req = UAVStateRequest()
            req.state = "HOVER"
            self.request_state(req)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)

        self.t_hover = threading.Thread(target=self.hover)
        self.t_hover.start()
        print("[HOVER] Starting (Thread)")

        return {}

    def hover(self):
        delay = 1

        while self.state != "HOVER":
            print("[HOVER] Waiting...")
            time.sleep(1)

        print("[HOVER] ...done! ")


        while self.state == "HOVER":
            self.set_target_xyz(0,0,1,delay)

        print("[HOVER] Stopped, new state: {}".format(self.state))


    def start_circle(self, r):
        try:
            req = UAVStateRequest()
            req.state = "CIRCLE"
            self.request_state(req)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)

        self.t_circle = threading.Thread(target=self.circle)
        self.t_circle.start()
        print(">> Starting circle (Thread)")

        return {}

    def circle(self):
        sides = 360
        radius = 1
        i = 0
        delay = 0.1

        while self.state != "CIRCLE":
            print("[CIRCLE] Waiting...")
            time.sleep(1)

        print("[CIRCLE] ...done! ")

        while self.state == "CIRCLE":
            x = radius * math.cos(i*2*math.pi/sides)
            y = radius * math.sin(i*2*math.pi/sides)
            z = self.target.pose.position.z

            self.set_target_xyz(x,y,z,delay)

            i = i + 1

            # Reset counter
            if(i > 360):
                i = 0

        print("[CIRCLE] Stopped, new state: {}".format(self.state))
        #self.set_state("RUNNING")


    def start_square(self, r):
        try:
            req = UAVStateRequest()
            req.state = "SQUARE"
            self.request_state(req)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)

        self.t_square = threading.Thread(target=self.square)
        self.t_square.start()
        print("[SQUARE] Starting (Thread)")

        return {}

    def square(self):
        delay = 5
        i = 0

        while self.state != "SQUARE":
            print("[SQUARE] Waiting...")
            time.sleep(1)

        print("[SQUARE] ...done! ")


        while self.state == "SQUARE":
            x = self.poly.exterior.coords[i][0]
            y = self.poly.exterior.coords[i][1]
            z = self.target.pose.position.z

            self.set_target_xyz(x,y,z,delay)

            i = i + 1

            # Reset counter
            if(i > 3):
                i = 0

        print("[SQUARE] Stopped, new state: {}".format(self.state))

    def stop(self,r):
        self.set_target_xyz(0,0,1,0.5)

        try:
            req = UAVStateRequest()
            req.state = "IDLE"
            self.request_state(req)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)

        return {}

if __name__ == '__main__':
    Etm = Endurance()
