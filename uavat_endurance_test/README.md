# UAVAT Framework: Endurance test
The aim of the endurance test module is to test the reliability of the sUAS and how it performs under stress scenarios such as different loads and maneuvers. 

## Feature model
When performent the endurance test, the following configurations must be considered:

- dynamic (optional), determining what flight dynamics to include in the test such as flight pattern, altitudes, and heading; 
- load (mandatory), determining the weight for the sUAS to carry, and thereby controlling the load put on the system during the test; and
- duration (mandatory), determining the desired duration of the endurance test. 

![Screenshot](images/feature_model.png)

## Endurance test (only)

```sh
rosrun uavat_endurance_test endurance.py
```

## Bringup

### PX4 SITL
```sh
roslaunch uavat_bringup bringup_endurance_sitl.launch
```

### Real-world
```sh
roslaunch uavat_bringup bringup_endurance.launch
```

### Activate flight patterns
The implementation of the endurance test module contains the following flight pattern modes:

### Hover mode
Hover, which is the default flight pattern mode, where the target setpoint is defined as the local position (0,0,1); 

```sh
rosservice call /setpoint_controller/hover "{}"
```

### Cirlce 
Circle (fixed heading), where the target setpoints are defining a circular path around (0,0,1) in the xy-plane with a radius of 1 meter. 

```sh
rosservice call /setpoint_controller/circle "{}"
```

### Square
Square (fixed heading), where the target setpoints are defined as four corners [(0.5, 0.5, 1), (0.5, -0.5, 1), (-0.5, -0.5, 1), (-0.5, 0.5, 1)]

```sh
rosservice call /setpoint_controller/square "{}"
