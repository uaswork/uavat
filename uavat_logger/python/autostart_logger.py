#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
autostart_logger.py: Starts and stops the ROSbag logger(s) based on the armed state

"""


###############################################
# Standard Imports                            #
###############################################
import sys
import time


###############################################
# ROS Imports                                 #
###############################################
import rospy
import rospkg

###############################################
# ROS Topic messages                          #
###############################################
from std_msgs.msg import Bool

from mavros_msgs.msg import State

###############################################
# Flarm Reader wrapper                        #
###############################################
class AutoStartLogger:
    def __init__(self, *args):
        rospy.init_node('pingrx_reader_server')
        self.rate = rospy.Rate(20)

        self.prefix = "[AutoStartLogger] "

        self.debounce = False

        self.db_msg = Bool()

        # Subscribers
        self.state_sub = rospy.Subscriber('/mavros/state', State, self.cb_state)

        # Publisher(s)
        self.pub_rf_optitrack = rospy.Publisher('/optitrack_logger/record_flag', Bool, queue_size=10)
        self.pub_rf_uav = rospy.Publisher('/uav_logger/record_flag', Bool, queue_size=10)

        # Spin until the node is stopped
        rospy.spin()

    """
    Callbacks
    * cb_state
    """
    def cb_state(self,state):
        if(state.armed == True and self.debounce != state.armed):
            print(self.prefix+"{}".format(state.armed))

            self.debounce = state.armed

            self.db_msg.data = self.debounce
            self.pub_rf_optitrack.publish(self.db_msg.data)
            self.pub_rf_uav.publish(self.db_msg.data)
        elif(state.armed == False and self.debounce != state.armed):
            print(self.prefix+"{}".format(state.armed))
            self.debounce = state.armed

            self.db_msg.data = self.debounce
            self.pub_rf_optitrack.publish(self.db_msg.data)
            self.pub_rf_uav.publish(self.db_msg.data)
            
if __name__ == '__main__':
    ASL = AutoStartLogger()
