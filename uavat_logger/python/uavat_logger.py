#!/usr/bin/env python3
################################################################################
#
# Copyright (c) 2019, Martin Peter Christiansen <mapc@mmmi.sdu.dk>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################
"""
2019-09-02 MPC First version
2019-09-04 MPC Adding comments and variable topic selection
2019-09-18 MPC Added yaml startup file and allow for multiple version of the node
2019-10-02 VKT Added zip methods and cleanup upon rosbags, also changed name to uavat_logger
2019-10-22 JEGJ Record all flag fixed, start-stop-start issue fixed and zipping+deleting *.bag.active added
2020-01-09 JEGJ Correct termination of the rosbag record, avoid getting the *.bag.active file + losing the information in the buffer
2020-01-16 JEGJ Ensure that the uavat_logger_bag node has closed, before zip and delete
2020-05-05 JEGJ Unique namemin added, so multiple loggers can run for endurance testing, eg. longterm and shorterm logging.
"""

# imports
import os
import sys
import shlex
import subprocess
import rospy
import rosnode
import zipfile
import signal
import time
from datetime import datetime
from itertools import compress
from std_msgs.msg import Bool
from std_msgs.msg import String

################################################################################
# Throttle Rosbag messages to a maximum update rate
# on another topic ending on *throttle.
# Where * respesents the current topic name
# These new throttle down topics can be used to limit
# the amount recorded data for each ROS topic.
class ThrottledRosTopics():
    ############################################################################
    def __init__(self, throttle_all, throttle_rate):
        self.throttle_active = False
        self.throttle_all = throttle_all
        self.throttle_rate = throttle_rate
        self.set_variables()
    ############################################################################

    ############################################################################
    def set_variables(self):
        if(self.throttle_all): #if everythin is
            self.msg_throttle_setup = True
        else:
            self.msg_throttle_setup = False

        self.topics_throttled = []
        self.throttled_subprocess = []
        # the following topics is never throtteled on this system
        self.topics_not_throttled = ['/rosout', '/rosout_agg', '/tf', '/clock']
        self.throttle_check_str = '_throttle'
        # the actual ROS node used to throttle down a topic
        self.throttle_cmd = '''rosrun topic_tools throttle messages '''
    ############################################################################

    ############################################################################
    def find_relevant_topics(self):
        if(self.throttle_all):
            # creates a list of topics to republish with throttle
            topic_list = [(elm[0], self.throttle_rate) for elm in rospy.get_published_topics()  \
                            # the topics we do not want to republish
                            if (elm[0] not in self.topics_not_throttled) and  \
                            # the topics preconfigured to be republished
                            (elm[0] not in self.throttle_topic_list) and  \
                            # do republish a topic containing the '_throttle' name
                            (self.throttle_check_str not in elm[0]) and \
                            # combinations like /pony/1 or /camera/4 cannot be mapped and is ignored
                            # Characters [1-9] is not valid as the first character in Graph Resource Name.
                            # Valid characters are a-z, A-Z, / and in some cases ~.
                            ((elm[0])[-2:-1]) != '/' and (not ((elm[0])[:-1]).isdigit())]
        else:
            topic_list = []

        return topic_list
    ############################################################################

    ############################################################################
    def setup_throttle(self, throttle_topic_list, max_update_rate):
        self.throttle_topic_list = throttle_topic_list
        if(len(throttle_topic_list)==len(max_update_rate)):
            self.external_topic_list = zip(throttle_topic_list, max_update_rate)
        else:
            rospy.loginfo("Not a correctly defined list of topics and rates for throttle")
            self.external_topic_list = []

        self.topics_throttled = list(self.external_topic_list)+self.find_relevant_topics()

        for topic in self.topics_throttled:
            cmd = self.throttle_cmd + ' ' + topic[0] + ' ' + str(1./topic[1])
            self.throttled_subprocess.append(  \
                                    subprocess.Popen(shlex.split(cmd),  \
                                    stdin=subprocess.PIPE, shell=False))

        self.msg_throttle_setup = True
        self.throttle_active = True
    ############################################################################

    ############################################################################
    # This function is used to check if new topics have appeared.
    # This is only done if the class it set to monitor every topic.
    def update_throttle(self):
        topic_list = self.find_relevant_topics()

        if(len(self.topics_throttled) != len(topic_list)):
            new_topics = list(set(self.topics_throttled) - set(topic_list))
            for topic in new_topics:
                if(topic not in self.topics_throttled):
                    cmd = self.throttle_cmd + ' ' + topic[0] + ' ' + str(1./topic[1])
                    self.throttled_subprocess.append(  \
                                            subprocess.Popen(shlex.split(cmd),  \
                                            stdin=subprocess.PIPE, shell=False))
                    self.topics_throttled.append(topic)
    ############################################################################

    ############################################################################
    def stop_throttles(self):
        for process in self.throttled_subprocess:
            # this should normally stop the process
            if(process.poll()!=0):
                process.kill()
            else:
                process.terminate()
            process.wait()

        # get the running node list
        node_names = rosnode.get_node_names()
        rosnode.kill_nodes(list(compress(node_names,['_throttle' in name for name in node_names])))

        self.msg_throttle_setup = False
        self.throttle_active = False
    ############################################################################

################################################################################

################################################################################
# The main class that controls everthing in terms of recording
#
class RosbagViaNode():
    ############################################################################
    def __init__(self):
        self.set_variables()
        self.configure_publishers()
        self.configure_subscribers()
        #update the Rosbag recording each 2 second
        self.timer = rospy.Timer(rospy.Duration(2.),self.on_timer)
        self.rosbag_cnt = 0

        self.node_exists = True
    ############################################################################

    ############################################################################
    # Set the rosparameters in the launch file
    # Setup rosbag command for recording
    def set_variables(self):
        # Init throttle as a empty element
        self.throttle_node = None

        # get the running node name
        self.node_name = rospy.get_name()

        # The naming start of the rosbags
        #self.rosbag_naming = self.node_name.replace('/', '')
        self.rosbag_naming = rospy.get_param("~rosbag_name", "uavat_logger")
        self.record_cmd = '''rosbag record -o ''' + self.rosbag_naming + ''' '''

        # the directory where the rosbags will be saved (default "")
        self.record_dir = rospy.get_param("~record_dir","")
        # If record_dis is "" use current work directory (cwd)
        if(self.record_dir == ""):
            self.record_dir = os.getcwd()

        # determines if all all pressent topics should be recorded
        self.record_all = rospy.get_param("~record_all",True)
        # determine if topics are recorded with there normal publish rate
        self.record_full = rospy.get_param("~record_full",False)
        # Parameter determine the max rate (Hz), unspecified topics are recorded with
        self.record_max_rate = rospy.get_param("~record_max_rate", 2.0)

        ########################################################################
        # load the topic parameters from the yaml
        param_record_list = [rosparam for rosparam in rospy.get_param_names() if rosparam.startswith(self.node_name) and "topic_list/" in rosparam]
        param_record_list.sort()
        self.record_list = []
        for i in range(0,len(param_record_list),2):
            if("down_scaler" in param_record_list[i] and "topic_name" in param_record_list[i+1]):
                    self.record_list.append([rospy.get_param(param_record_list[i+1]), rospy.get_param(param_record_list[i])])
        ########################################################################

        print(self.record_list)

        if(self.record_full and self.record_all):
            self.record_cmd += '''-a ''' # we want to record everything
        elif(self.record_full and (not self.record_all)):
            for elm in self.record_list:
                self.record_cmd += str(elm[0]) +''' '''
        else:
            self.record_cmd += '''/rosout_agg /rosout -e "(.*)throttle" '''
            self.throttle_node = ThrottledRosTopics(self.record_all, self.record_max_rate)
            self.throttle_node.setup_throttle([t[0] for t in self.record_list],[t[1] for t in self.record_list])

        print(self.record_cmd)

        # Determine if the recorded rosbags are published on a topic
        self.publish_rosbags = rospy.get_param("~publish_rosbags",False)
        # split the rosbag into smaller chunks
        self.rosbag_split = rospy.get_param("~rosbag_split", False)
        # the chosen duration of each sub rosbag (in seconds)
        self.rosbag_split_duration = rospy.get_param("~rosbag_split_duration",30) # value in seconds
        # number of rosbags before deletion (0 == infinity)
        self.rosbag_limit = rospy.get_param("~rosbag_limit", 0)
        # Variable determine if the recording is active
        self.active = False
        # The sub process doing the recording
        self.recording_process = None

        self.rosbag_name = rospy.get_param("~rosbag_name", "uavat_logger_bag")

        if(self.rosbag_split):
            self.record_cmd += '''--split --duration ''' + str(self.rosbag_split_duration) +''' '''
            if(self.rosbag_limit > 0):
                self.record_cmd += '''--max-splits=''' + str(self.rosbag_limit) +''' '''

        self.bags = []

        # Name the record_* node, so that we can find and close it down correctly
        self.record_cmd += '''__name:='''+self.rosbag_naming+'''_bag '''
        rospy.loginfo("Full rosbag cmd: {}".format(self.record_cmd))

    ############################################################################

    ############################################################################
    def configure_subscribers(self):
        self.record_flag_sub = rospy.Subscriber("~record_flag",Bool,self.on_record_flag)
    ############################################################################

    ############################################################################
    def configure_publishers(self):
        self.status_pub = rospy.Publisher("~recording_active", Bool,queue_size=5)
        self.rosbags_pub = rospy.Publisher("~rosbags_in_folder",String,queue_size=2)
    ############################################################################

    ############################################################################
    def on_timer(self,event):
        self.status_pub.publish(self.active)
        self.get_rosbags()
        if(self.rosbag_limit > 0 and len(self.bags) > self.rosbag_limit):
            os.remove(self.bags[0]+'.bag')

        if(self.throttle_node != None):
            self.throttle_node.update_throttle()

        if(self.publish_rosbags):
            self.rosbags_pub.publish(",".join(self.bags))
    ############################################################################

    ############################################################################
    def on_record_flag(self,msg):
        if((not(self.active)) and msg.data):
            #if(not self.msg_throttle_setup):
            #    self.setup_throttle()
            self.start_recording()

        if(self.active and (not(msg.data))):
            self.stop_recording()
    ############################################################################

    ############################################################################
    def start_recording(self):

        try:
            rospy.loginfo("Recording in : " + self.record_dir)
            self.recording_process = subprocess.Popen(  \
                                    shlex.split(self.record_cmd),  \
                                    stderr=subprocess.PIPE, \
                                    stdin=subprocess.PIPE,  \
                                    shell=False,  \
                                    cwd=self.record_dir)
            self.active = True
        except:
            print("Unexpected error: {}".format(sys.exc_info()[0]))
            rospy.logerror("Unexpected error:", sys.exc_info()[0])
            raise
    ############################################################################

    ############################################################################
    def get_ros_nodes(self):
        nodes = os.popen("rosnode list").readlines()
        for i in range(len(nodes)):
            nodes[i] = nodes[i].replace("\n","")

        return nodes


    def stop_recording(self):
        rospy.loginfo("Stopping /"+self.rosbag_naming+"_bag recording node")

        # Get all existing node, find the uavat_logger_bag node and shut it down
        nodes = self.get_ros_nodes()
        for node in nodes:
            #print("Name -> {}".format(node))
            if node == "/"+self.rosbag_naming+"_bag":
                os.system("rosnode kill "+ node)


        # Ensure that it has closed, before Terminating the subprocess
        self.node_exists = True
        while self.node_exists:
            self.node_exists = False
            nodes = self.get_ros_nodes()

            for node in nodes:
                #print("Name -> {}".format(node))
                if node == "/"+self.rosbag_naming+"_bag":
                    self.node_exists = True

            time.sleep(5)

        # Terminate
        rospy.loginfo("Terminating subprocess")
        self.recording_process.terminate()

        # Zip the bags, and clean up the files
        rospy.loginfo("Zip and cleanup")
        self.zip_bags_and_delete()

        self.active = False
    ############################################################################

    ############################################################################
    def close_node(self):
        #self.stop_recording()
        #os.kill(self.recording_process.pid, signal.SIGINT)
        if self.active == True:
            self.stop_recording()

        if(self.throttle_node != None):
            self.throttle_node.stop_throttles()
    ############################################################################

    ############################################################################

    def get_rosbags(self):
        sorted_bags = sorted([os.path.splitext(os.path.splitext(f)[0])[0]   \
                                for f in os.listdir(self.record_dir)  \
                                if os.path.isfile(os.path.join(self.record_dir,f))  \
                                and f.startswith(self.rosbag_naming)])
                                #and f.endswith(".bag")])

        if(len(sorted_bags) > 0):
            return sorted_bags
        else:
            return []
    ############################################################################

    ############################################################################
    def get_all_file_paths(self, directory):

        # initializing empty file paths list
        file_paths = []

        # crawling through directory and subdirectories
        for root, directories, files in os.walk(directory):
            for filename in files:
                # join the two strings in order to form the full filepath.
                filepath = os.path.join(root, filename)
                file_paths.append(filepath)

                # returning all file paths
        return file_paths
    ############################################################################

    ############################################################################
    def zip_bags_and_delete(self):
        directory = self.record_dir
        file_paths = self.get_all_file_paths(directory)
        # printing the list of all files to be zipped

        timestamp = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        # writing files to a zipfile
        with zipfile.ZipFile(self.record_dir+'/'+self.rosbag_naming+'_'+timestamp+'_'+str(self.rosbag_cnt)+'.zip',mode='w',allowZip64 = True) as zip:
        # writing each file one by one
            for file in file_paths:
                if file.endswith(".bag"):
                    zip.write(file,os.path.basename(file))
                if file.endswith(".bag.active"):
                    zip.write(file,os.path.basename(file))

        self.rosbag_cnt = self.rosbag_cnt + 1

        test = os.listdir(self.record_dir)
        for item in test:
            if item.endswith(".bag"):
                os.remove(os.path.join(self.record_dir, item))
            if item.endswith(".bag.active"):
                os.remove(os.path.join(self.record_dir, item))

        print('All files zipped successfully and rosbags removed!')

    ############################################################################

    ############################################################################
################################################################################


################################################################################
if __name__ == "__main__":
    # init the node with std name (multiple are allowed)
    rospy.init_node("uavat_logger")
    node = RosbagViaNode()
    rospy.spin()
    node.close_node() #shutdown the running threads
################################################################################
