# UAVAT Framework: ROSBag logger

## Bringup

### PX4 SITL
```sh
roslaunch uavat_logger sitl_logger.launch
```

### Real-world

#### Offboard logger
Long-term logging of the systems behavior. (see [config](resource/optitrack.yaml) file)

```sh
roslaunch uavat_logger optitrack_logger.launch
```

#### Onboard logger
Continuously keep a circular log of the last few minutes of the systems behavior. (see [config](resource/uav.yaml) file)

```sh
roslaunch uavat_logger uav_logger.launch
```
